APP=insecure-programming

all: build

build:
	kubectl create namespace $(APP)-ns
	kubectl create serviceaccount -n $(APP)-ns test-user
	kubectl create rolebinding -n $(APP)-ns test-editor --clusterrole=edit --serviceaccount=$(APP)-ns:test-user
	kubectl -n $(APP)-ns create -f policy.yaml
	kubectl -n $(APP)-ns create role psp:unprivileged --verb=use --resource=podsecuritypolicy --resource-name=example role "psp:unprivileged" created
	kubectl -n $(APP)-ns create rolebinding test-user:psp:unprivileged --role=psp:unprivileged --serviceaccount=$(APP)-ns:test-user rolebinding "test-user:psp:unprivileged" created

run:
	kubectl --as=system:serviceaccount:$(APP)-ns:test-user -n $(APP)-ns create -f deployment.yaml

clean:
	kubectl delete ns $(APP)-ns

.PHONY: all clean
